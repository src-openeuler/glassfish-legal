Name:                glassfish-legal
Version:             1.1
Release:             3
Summary:             Legal License for glassfish code
License:             CDDL-1.0 or GPLv2 with exceptions
URL:                 http://glassfish.java.net/
Source0:             https://repo1.maven.org/maven2/org/glassfish/legal/1.1/legal-1.1-sources.jar
Source1:             https://repo1.maven.org/maven2/org/glassfish/legal/1.1/legal-1.1.pom
BuildRequires:       glassfish-master-pom maven-local maven-remote-resources-plugin
Requires:            glassfish-master-pom
BuildArch:           noarch
%description
An archive which contains license files for glassfish code.

%prep
%setup -c %{name}-%{version}
mkdir -p src/main/resources
mv META-INF/ src/main/resources
cp %{SOURCE1} ./pom.xml
sed -i 's/\r//' src/main/resources/META-INF/LICENSE.txt
cp -p src/main/resources/META-INF/LICENSE.txt .
%mvn_file :legal %{name}

%build
%mvn_build -- -Dproject.build.sourceEncoding=UTF-8

%install
%mvn_install

%files -f .mfiles
%license LICENSE.txt

%changelog
* Mon Jun 27 2022 Xinyi Gou <plerks@163.com> - 1.1-3
- specify license version

* Wen Sept 2 2020 Ge Wang <wangge20@huawei.com> - 1.1-2
- change META-INF path to solve glassfish-annotation-api build problem

* Sat Aug 1 2020 Ge Wang <wangge20@huawei.com> - 1.1-1
- package init

